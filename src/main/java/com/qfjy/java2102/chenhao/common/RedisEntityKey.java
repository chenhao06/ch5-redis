package com.qfjy.java2102.chenhao.common;

public interface RedisEntityKey {

    String REDIS_ENTITY_KEY_USERPO="userpo";
    String REDIS_ENTITY_KEY_USER="user";
    String REDIS_ENTITY_KEY_USER_TIME="user:time";
}

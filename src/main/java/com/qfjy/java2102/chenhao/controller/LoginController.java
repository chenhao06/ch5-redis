package com.qfjy.java2102.chenhao.controller;

import com.alibaba.fastjson.JSON;
import com.qfjy.java2102.chenhao.service.impl.LoginServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@Slf4j
@RequestMapping("chenhao")
public class LoginController {

    @Autowired
    private LoginServiceImpl loginService;
    @GetMapping("toLogin")
    public String toLogin(){
        return "index";
    }

    @GetMapping("login")
    @ResponseBody
    public JSON login(@RequestParam(value = "user",required = false) String user, @RequestParam(value = "password",required = false)String password){
        boolean flag = loginService.checkUser(user);
        JSON msg = null;
        if (flag == false){
            String userfail = "{'fail':'用户不存在'}";
            msg = JSON.parseObject(userfail);
        }else {
            String userStatus = loginService.checkUserStatus(user, password);
            msg = JSON.parseObject(userStatus);
        }
        System.out.println(msg);
        return msg;
    }
}

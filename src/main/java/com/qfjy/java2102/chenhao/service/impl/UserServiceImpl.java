package com.qfjy.java2102.chenhao.service.impl;

import com.qfjy.entity.po.UserPO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @ClassName UserServiceImpl
 * @Description TODO
 * @Author guoweixin
 * @Date 2021/8/6
 * @Version 1.0
 */
@Service
@Slf4j
public class UserServiceImpl {
    /**
      设计模式：模版模式。

     JDBC-->自定义封装工具类：DBUtils   Spring官方框架---》JdbcTemplate

     服务间通信（HTTP）方式
      JDK原生URLConnection-->自定义封装URL工具类 WeixinUtil
      --->Apache HttpClient  Spring官方框架--》RestTemplate
     SolrClient也是封闭（URLConnection)

     JAVA操作Redis命令---》原生 技术：Jedis
     ： Jedis---》JedisUtil(自定义封装）/ SpringBoot框架 RedisTemplate


     */




    public void testRedis(){
        //redisTemplate.opsForValue().set("java-ch5-redis","java-ch5-springboot整合Redis实例");
        Object obj=redisTemplate.opsForValue().get("java-ch5-redis");
        System.out.println(obj);
        System.out.println(redisTemplate);
    }

    /**
     * JDBCTemplate是对JDBC封装
     * RestTemplate是对URLConnection封装
     * RedisTemplate是Jedis封装
     *  Redis有什么命令，Jedis就有什么方法。 Redis命令====Jedis方法
     *  RedisTemplate很多方法就和Redis命令就不样了（还是很直观很简单）。
     */

    /*
      RedisTemplate模拟 String类型存取过程
        redisTemplate.expire(key,10, TimeUnit.HOURS);
     */

    @Autowired
    private RedisTemplate<String,Object> redisTemplate;

    /**
     * string值是根据redisTemplate名称来赋值
     * string==redisTemplate.opsForValue()
     */
    @Resource(name="redisTemplate")
    private ValueOperations<String,String> string;

    @Resource(name = "redisTemplate")
    private HashOperations<String, String, UserPO>  hash;


    public String getString(String key){

        if(redisTemplate.hasKey(key)){
            log.info("查询的是Redis");

           return string.get(key);

        }else{

            log.info("查询的是MYSQL");
            String result="JAVA-2102-班";

            string.set(key,result);

            return result;
        }

    }

    /**
     * RedisTemplate 模拟 hash类型存取过程
     */


    public UserPO selectUserPOById(String id){
       // String key="userpo:"+id;

        if(redisTemplate.opsForHash().hasKey(UserPO.keyName(),id)){
            log.info("查询的是Redis中的对象");

          return   hash.get(UserPO.keyName(),id);
        }else{
            log.info("查询的是MYSQL");
            UserPO userPO=new UserPO();
            userPO.setId(id);
            userPO.setName("王凯歌");
            userPO.setAge(18);

            hash.put(UserPO.keyName(),id,userPO);

            return userPO;
        }

    }
}

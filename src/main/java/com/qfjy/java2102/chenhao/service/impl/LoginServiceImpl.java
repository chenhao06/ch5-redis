package com.qfjy.java2102.chenhao.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

import java.util.Map;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import static com.qfjy.java2102.chenhao.common.RedisEntityKey.REDIS_ENTITY_KEY_USER;
import static com.qfjy.java2102.chenhao.common.RedisEntityKey.REDIS_ENTITY_KEY_USER_TIME;

@Service
public class LoginServiceImpl {

    @Autowired
    private RedisTemplate<String,Object> redisTemplate;


    @Resource(name="redisTemplate")
    private ValueOperations<String,String> str;

    /**1.校验账号是否存在。
     1.1 如果账号不存在，则返回信息“用户不存在”
     * 校验用户是否存在
     * @param user 前端传入的用户名
     * @return  true 存在 false 不存在
     */
    public boolean checkUser(String user){
        if (redisTemplate.hasKey(user)){
            return true;
        }else {
            return false;
        }
    }

    /**
     *  2.检查用户可登录限制数据key="user:time:12306"（模拟账号为12306，下同）是否存在于redis库中。
     *         2.1如果存在，则返回当前剩余限制登录时间。
     *         2.2如果不存在，则判断该用户是否登录过
     *             2.2.1如果没登录过，则为其创建key="user:12306"，value = 0 的数据。
     *             2.2.2进行校验密码
     * @param user 前端传入的用户名
     * @return
     */
    public String checkUserStatus(String user,String password){
        String keyUser =  REDIS_ENTITY_KEY_USER + ":" + user;
        String keyUserTime = REDIS_ENTITY_KEY_USER_TIME + ":" + user;
        if (redisTemplate.hasKey(keyUserTime)){
            String loginTime = String.valueOf(redisTemplate.getExpire(keyUserTime));
            return "{'fail':'请在"+ loginTime +"秒后再登录'}";
        }else {
            if (!redisTemplate.hasKey(keyUser)){
                str.set(keyUser,"0");
            }
            String msg = checkPassword(user,password,keyUser,keyUserTime);
            return msg;
        }
    }

    /**
     * 3校验密码。
     *         3.1密码正确返回信息“登录成功”。
     *         3.2密码不正确
     *             3.2.1检查key="user:12306"的value的值
     *                 3.2.1.1如果为0,则为key="user:12306"的数据设置存在时间为2分钟。
     *                 3.2.1.2如果=5,则创建一个key="user:time:12306",value=0，设置存在时间为1小时。
     *                 3.2.1.3否则key="user:12306"的value自增1。并返回当前剩余时间，以及剩余允许错误的次数。
     * @param user  前端传入的用户名
     * @param password 前端传入的密码
     * @param keyUser 用户输错密码次数键
     * @param keyUserTime 用户限制登录时间键
     * @return
     */
    public String checkPassword(String user, String password, String keyUser , String keyUserTime){
        if (password.equals(str.get(user))){
            return "{'suc':'登录成功'}";
        }else {
            if ("0".equals(str.get(keyUser))){
                str.set(keyUser,"1",120,TimeUnit.SECONDS);
            }else if ("4".equals(str.get(keyUser))){
                str.set(keyUserTime,"0",1,TimeUnit.HOURS);
                String loginTime = String.valueOf(redisTemplate.getExpire(keyUserTime));
                return "{'fail':'请在"+ loginTime +"小时后再登录'}";
            }
            str.increment(keyUser,1);  //报错：ERR value is not an integer or out of range
            String count = String.valueOf(5 - Integer.parseInt (str.get(keyUser)));
            String time = String.valueOf(redisTemplate.getExpire(keyUser));
            return "{'err':'密码错误，您在"+ time +"秒内，还有"+ count +"登录机会'}";
        }
    }

}
